import Vue from 'vue'
import VueRouter from 'vue-router'
import Index from '@/components/Index.vue'
import Simple from '@/components/Simple.vue'
import List from '@/components/List.vue'
import Object from '@/components/Object.vue'

Vue.use(VueRouter)

const routes = [
  {path: '/', component: Index},
  {path: '/simple/', component: Simple},
  {path: '/list/', component: List},
  {path: '/object/', component: Object}
]

export default new VueRouter({
  mode: 'history',
  routes: routes
})